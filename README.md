Suivre les tutos de airplug chez https://airplug-team.hds.utc.fr/en/doc/tuto/start

Lancement de AirPlug
    Configurer la solution

        cd ~/AIRPLUG/bin
        source config.sh

    **Test 3 sites**__
    Chaque BAS est connecté à un NET
    Et les NET sont reliés en anneau NET1 > NET 2 > NET3 > NET 1

mkfifo /tmp/in1a /tmp/out1a
mkfifo /tmp/in1b /tmp/out1b
mkfifo /tmp/in2a /tmp/out2a
mkfifo /tmp/in2b /tmp/out2b
mkfifo /tmp/in3a /tmp/out3a
mkfifo /tmp/in3b /tmp/out3b
./bas.py --auto --ident=J1 --nbJ=3 --whatwho --dest=NET < /tmp/in1a > /tmp/out1a &
./net.py --auto --ident=J1 --nbJ=3 --whatwho --source=BAS < /tmp/in1b > /tmp/out1b &
./bas.py --auto --ident=J2 --nbJ=3 --whatwho --dest=NET < /tmp/in2a > /tmp/out2a &
./net.py --auto --ident=J2 --nbJ=3 --whatwho --source=BAS < /tmp/in2b > /tmp/out2b &
./bas.py --auto --ident=J3 --nbJ=3 --whatwho --dest=NET < /tmp/in3a > /tmp/out3a &
./net.py --auto --ident=J3 --nbJ=3 --whatwho --source=BAS < /tmp/in3b > /tmp/out3b &
cat /tmp/out1a > /tmp/in1b &
cat /tmp/out1b | tee /tmp/in1a > /tmp/in2b &
cat /tmp/out2a > /tmp/in2b &
cat /tmp/out2b | tee /tmp/in2a > /tmp/in3b &
cat /tmp/out3a > /tmp/in3b &
cat /tmp/out3b | tee /tmp/in3a > /tmp/in1b
