# Installation AirPlug

## Télécharger les archives BASpy, NETpy et Airplug depuis Moodle

https://airplug-team.hds.utc.fr/en/dwl/chicken-baspy/content/accept

https://airplug-team.hds.utc.fr/en/dwl/chicken-netpy/content/accept

https://airplug-team.hds.utc.fr/en/dwl/nest/content/accept


## Pré-requis 

Sur un système linux (DEBIAN) :
```shell
sudo apt-get install tcl tk
sudo apt-get install python3-sortedcontainers python3-tk python3-uvloop
```

Pour les icônes :
```
sudo apt-get install tgif
sudo apt-get install xfonts-100dpi xfonts-75dpi
```
## Installation

Créer un répertoire AIRPLUG
```
mkdir ~/AIRPLUG
cd ~/AIRPLUG
``` 

Y copier les 3 archives puis les dézipper
```
tar xvzf airplug*skl*tgz
tar xvzf airplug*bas*tgz
tar xvzf airplug*net*tgz
``` 

Lancer l'installation
```
make install
``` 

Configuration
```
cd bin
source config.sh
```

# Focus sur NET et BAS

## Créer une nouvelle APP NET et BAS

Soit avec le script :
```
bin/newapp.sh -v -i=NETpy -o=CTRpy
bin/newapp.sh -v -i=BASpy -o=WUMpy
```

Soit avec les commandes classiques :
```
mkdir CTRpy
mkdir CTRpy/CTRpy-v0.1
ln -s CTRpy-v0.1 -T CTRpy/CTRpy
cp -r NETpy/* CTRpy
cp -r NETpy/NETpy/* CTRpy/CTRpy

cd CTRpy/CTRpy
for F in rc-net.py net.py net-???.py; do mv $F `echo $F | sed s/net/ctr/` ; done

mkdir WUMpy
mkdir WUMpy/WUMpy-v0.1
ln -s WUMpy-v0.1 -T WUMpy/WUMpy
cp -r BASpy/* WUMpy
cp -r BASpy/BASpy/* WUMpy/WUMpy

cd WUMpy/WUMpy
for F in rc-bas.py bas.py bas-???.py; do mv $F `echo $F | sed s/bas/wum/` ; done
```

## Renommage

### ... des variables
Renommer les variables avec précaution

Soit avec le script suivant :
```
bin/replacestr.sh -v -i=net -o=ctr -s CTRpy/CTRpy/rc-ctr.py CTRpy/CTRpy/ctr.py CTRpy/CTRpy/ctr-???.py

bin/replacestr.sh -v -i=bas -o=wum -s WUMpy/WUMpy/rc-wum.py WUMpy/WUMpy/wum.py WUMpy/WUMpy/wum-???.py
```

Soit avec les commandes suivantes :
```
for F in rc-ctr.py ctr*py ; do sed -ie "s/net/ctr/g" ; done
for F in rc-ctr.py ctr*py ; do sed -ie "s/NET/ctr/g" ; done

for F in rc-wum.py wum*py ; do sed -ie "s/bas/wum/g" ; done
for F in rc-wum.py wum*py ; do sed -ie "s/BAS/wum/g" ; done
```

Ensuite, vérifier les replacements "abusifs" :
```
diff -ybB --suppress-common-lines CTRpy/CTRpy/ctr.py CTRpy/CTRpy/ctr.py.old | more

diff -ybB --suppress-common-lines WUMpy/WUMpy/wum.py WUMpy/WUMpy/wum.py.old | more
```

Restorer si besoin :
```
cp ctr-rcv.py.old ctr-rcv.py
cp wum-rcv.py.old wum-rcv.py
```

### ... des strings

Pareil avec les string à remplacer :
```
bin/replacestr.sh -v -i=NET -o=CTR -s CTRpy/CTRpy/rc-ctr.py CTRpy/CTRpy/ctr.py CTRpy/CTRpy/ctr-???.py

bin/replacestr.sh -v -i=BAS -o=WUM -s WUMpy/WUMpy/rc-wum.py WUMpy/WUMpy/wum.py WUMpy/WUMpy/wum-???.py
```

## Suppression des copies

Quand tout est bon, on supprime les copies :
```
rm CTRpy/CTRpy/*.old
rm WUMpy/WUMpy/*.old
```

## Touches finales

**HEADERS**

Modifier les headers, bien respecter le format car utilisé par AIRPLUG
```
#    hlg
#    an airplug compatible program
#    author: Your name here, hlg from NET-v0.31
#    license type: free of charge license for academic and research purpose
#    see license.txt
```

``` 
make show-version
```

**ICÔNES**
```
cd CTRpy
rm -f CTRpy/icons/*
make icon

cd WUMpy
rm -f WUMpy/icons/*
make icon
```

## Installer et tester

``` 
make install

cd ../bin
source config.sh
./ctr.py --verbose=5

./wum.py --verbose=5

./wum.py --whatwho --ident=writer --auto --bas-delay=1000 --bas-autosend --bas-dest=CTR | \
./ctr.py --whatwho --ident=writer --auto | \
./ctr.py --whatwho --ident=reader --auto | \
./wum.py --whatwho --ident=reader --auto --begin=CTR
```

```
./wum.py --whatwho --ident=writer --auto --bas-delay=1000 --bas-autosend --bas-dest=CTR --verbose | \
./ctr.py --whatwho --ident=writer --auto | \
./ctr.py --whatwho --ident=reader --auto | \
./wum.py --whatwho --ident=reader --auto --begin=CTR --verbose
```

## 2 Joueurs

```shell
mkfifo /tmp/in1a /tmp/out1a
mkfifo /tmp/in1b /tmp/out1b
mkfifo /tmp/in2a /tmp/out2a
mkfifo /tmp/in2b /tmp/out2b
 
./wum.py --n=10 --nbJ=2 --ident=J1 --whatwho --dest=NET --verbose < /tmp/in1a > /tmp/out1a &
./net.py --ident=J1 --whatwho --source=BAS < /tmp/in1b > /tmp/out1b &
 
./wum.py --n=10 --nbJ=2 --ident=J2 --whatwho --dest=NET --verbose < /tmp/in2a > /tmp/out2a &
./net.py --ident=J2 --whatwho --source=BAS < /tmp/in2b > /tmp/out2b &
 
cat /tmp/out1a > /tmp/in1b &
cat /tmp/out1b | tee /tmp/in1a > /tmp/in2b &
cat /tmp/out2a > /tmp/in2b &
cat /tmp/out2b | tee /tmp/in2a > /tmp/in1b
```

## 3 Joueurs

```shell
mkfifo /tmp/in1a /tmp/out1a
mkfifo /tmp/in1b /tmp/out1b
mkfifo /tmp/in2a /tmp/out2a
mkfifo /tmp/in2b /tmp/out2b
mkfifo /tmp/in3a /tmp/out3a
mkfifo /tmp/in3b /tmp/out3b
 
./wum.py --n=10 --nbJ=3 --ident=J1 --whatwho --dest=NET < /tmp/in1a > /tmp/out1a &
./net.py --ident=J1 --whatwho --source=BAS < /tmp/in1b > /tmp/out1b &
 
./wum.py --n=10 --nbJ=3 --ident=J2 --whatwho --dest=NET < /tmp/in2a > /tmp/out2a &
./net.py --ident=J2 --whatwho --source=BAS < /tmp/in2b > /tmp/out2b &
 
./wum.py --n=10 --nbJ=3 --ident=J3 --whatwho --dest=NET < /tmp/in3a > /tmp/out3a &
./net.py --ident=J3 --whatwho --source=BAS < /tmp/in3b > /tmp/out3b &
 
cat /tmp/out1a > /tmp/in1b &
cat /tmp/out1b | tee /tmp/in1a > /tmp/in2b &
cat /tmp/out2a > /tmp/in2b &
cat /tmp/out2b | tee /tmp/in2a > /tmp/in3b &
cat /tmp/out3a > /tmp/in3b &
cat /tmp/out3b | tee /tmp/in3a > /tmp/in1b
```