# Lancement 

## ... de AirPlug 
Configurer la solution
```
cd ~/AIRPLUG/bin
source config.sh
```

Tester avec une appli simple
```
./wha.tk
hello world<enter>

./wha.tk | ./wha.tk
```

Créer un anneau simple
```
mkfifo /tmp/fifo1 /tmp/fifo2
./wha.tk  < /tmp/fifo1 > /tmp/fifo2 &
./wha.tk  < /tmp/fifo2 > /tmp/fifo1 &
echo "start" > /tmp/fifo1
```

## ...de BAS et NET indépendamment

Tester une application BAS simple
```
./bas.py
```

Paramètres de la ligne de commande :
- `-default-pld=msg` : Message par défaut à envoyer périodiquement
- `-bas-delay=del` : Délai entre 2 messages (en secondes)
- `–bas-dest=APP` : Application de destination - par défaut, BAS. 'ALL' existe.
- `–bas-autosend` : Débute l'envoi périodique automatiquement au lancement de l'appli, sans appuyer sur le bouton "Auto"

Communication entre 2 BAS (un envoi, l'autre reçoit) - juste what obligatoire :
```
./bas.py --ident=writer --auto | ./bas.py --ident=reader --auto 
```


Tester une application NET simple
```
./net.py
```

## ... de BAS et NET ensemble

Tester une application BAS et NET (whatwho obligatoire) :
```
./bas.py --whatwho --ident=writer --auto --bas-delay=1000 --bas-dest=NET --bas-autosend --verbose=5 | \
./net.py --whatwho --ident=writer --auto --verbose=5 | \
./net.py --whatwho --ident=reader --auto --verbose=5 | \
./bas.py --whatwho --ident=reader --auto --verbose
```

```
./bas.tk --whatwho --ident=writer --auto --bas-delay=1000 --bas-dest=NET --bas-autosend | \
./net.tk --whatwho --ident=writer --auto | \
./net.tk --whatwho --ident=reader --auto | \
./bas.tk --whatwho --ident=reader --auto
```

_Remark : Instead of using pipes (|), we may use so-called named pipes, which are pipes having an entry and a name in the file system. This allows using these pipes for loops, as shown in the example below with a ring of two WHA application_

# Options d'airplug

**Afficher des informations**

`--verbose` / `--verbose=4` / `--debug`: Display more information in the terminal

`--auto=0` / `--auto` : Start the application automatically

**Redimensionner la fenêtre**

`-geometry +/-X x/i Y` : + OU - et X, Y des nombres entiers

Exemples :
- Top left corner of the screen
```
./wha.tk -geometry +0+0
```
- 50 pixels horizontally from the top left corner
```
./wha.tk -geometry +50+0
```
- Top right corner of the screen
```
./wha.tk -geometry -0+0
```
- 90 pixels vertically from the bottom-left corner
```
./wha.tk -geometry +0-90
```
- Bottom-right corner of the screen
```
./wha.tk -geometry -0-0
```

**En cas d'erreur**

Trouver les pid et kill les processus
```
ps aux | grep wha

kill <process id> or kill -KILL <process id>
```

# Type d'application

## **WHAT**

`--what` : format de message WHAT

=> Just the payload

## **WHAT WHO**

`--whatwho` : format de message WHAT WHO

=> sender application + reiceiver application + payload + nseq

**Faire communiquer WHAT et WHO**

WHO n'accepte pas les WHAT messages car il ne s'est pas "abonné" à l'application.

Par défaut, une app whatwho parle à d'autres instances de who.tk.


Exemple :
```
# Ne marche pas
./wha.tk --whatwho --verbose | ./who.tk --whatwho

# Marche
/wha.tk --whatwho --verbose --appname=WHO --ident=sender | ./who.tk --whatwho --ident=receiver
```


## **WHAT WHO WHERE**

`--whatwhowhere` : format de message WHAT WHO WHERE

=> sender application + reiceiver application + area of distribution + payload


**Faire communiquer WHO et WHERE**

WHE envoie des messages à ALL (Jocker précisé en haut) MAIS Who n'étant pas abonné à WHE (comme avant), les messages de WHE seront ignorés chez les WHO.

Il faut donc souscrire encore une fois. Cela peut se faire via l'interface graphique dans l'exemple ou en ligne de commandes.


Exemple :
```
# Ne marche pas
./whe.tk --whatwhowhere |./who.tk --whatwhowhere

# Marche
./whe.tk --whatwhowhere |./who.tk --whatwhowhere --begin=WHE
```

# Construction de réseau

## **Chaîne unidirectionnel**

Network WHA → WHA → WHA → WHA → WHA
```
./wha.tk --ident=first | ./wha.tk --ident=second | ./wha.tk --ident=third | ./wha.tk --ident=fourth | ./wha.tk --ident=fivth
```

## **Lien bi-directionnel**

On utilise les named pipe = UNIX FIFO

Avec 2 applications :

```
mkfifo /tmp/F
./wha.tk --ident=first/ < tmp/F | ./wha.tk --ident=second > /tmp/F
```

Avec 3 :
```
mkfifo /tmp/F
./wha.tk --ident=first < /tmp/F | ./wha.tk --ident=second | ./wha.tk --ident=third > /tmp/F
``` 


## **1 APP envoie à plusieurs APP**

Les exemples précédents sont des liens 1 - 1. Maintenant, voici un lien 1 - 2 (un app envoie à 2 autres apps) (racine avec 2 fils) avec `tee` :
```
mkfifo /tmp/F
mkfifo /tmp/G
./wha.tk --ident=first_son < /tmp/F &
./wha.tk --ident=second_son < /tmp/G &
./wha.tk --ident=first | tee /tmp/F /tmp/G &
```

## Exemple de redirections entre les applis pour remplacer le |

IN 1 et OUT1 de APP1 / IN2 et OUT2 de APP2

Le OUT1 est redirigé vers le IN2
Le OUT2 et redirigé vers le IN1

```
mkfifo /tmp/in1 /tmp/out1
mkfifo /tmp/in2 /tmp/out2
 
./wha.tk --ident=first < /tmp/in1 > /tmp/out1 &
./wha.tk --ident=second < /tmp/in2 > /tmp/out2 &
 
cat /tmp/out1 > /tmp/in2 &
cat /tmp/out2 > /tmp/in1 &
```

**Identifier les liens**
Ex : obtenir le lien entre le 1er WHA vers le second (APP1 > APP2)

```
num=`ps aux | grep "cat /tmp/out1" | grep -v grep | tr -s ' ' | cut -d' ' -f2`

# OU

cat /tmp/out1 > /tmp/in2 &
pid_link1=$!
```

**Supprimer un lien identifié**

Directement :

```
kill -KILL $pid_link1
```

Après un certain temps :
```
sleep 10
```


## Anneau Uni-directionnel

```
mkfifo in1 in2 in3
mkfifo out1 out2 out3
./wha.tk --ident=first  < in1 > out1 &
./wha.tk --ident=second < in2 > out2 &
./wha.tk --ident=third  < in3 > out3 &
cat out1 > in2 &
cat out2 > in3 &
cat out3 > in1 &
```

On peut mettre la commande dans un script shell ring.sh et le run avec :
```
chmod +x ring.sh
./ring.sh
```

## Voisinnage

Quand une app envoi un message dans un voisinnage, toutes les app le reçoivent.

```
mkfifo f1 f2 f3
./wha.tk --ident=first  < f1 | tee f2 f3 &
./wha.tk --ident=second < f2 | tee f1 f3 &
./wha.tk --ident=third  < f3 | tee f1 f2 &

```

## Raccourci (Un graph avec un chemin plus court de 2 à 4 par ex sans passer par 3)

```
\rm -f in* out*
mkfifo in1 in2 in3 in4
mkfifo out1 out2 out3 out4
./wha.tk --auto --ident=first  < in1 > out1 &
./wha.tk --auto --ident=second < in2 > out2 &
./wha.tk --auto --ident=third  < in3 > out3 &
./wha.tk --auto --ident=fourth < in4 > out4 &
 
cat out1 > in2 &
cat out2 | tee in3 in4 &
cat out3 > in4 &
cat out4 > in1 &

```

## Anneau de BAS et NET

Chaque BAS est connecté à un NET
Et les NET sont reliés en anneau NET1 > NET 2 > NET3 > NET 1

```shell
mkfifo /tmp/in1a /tmp/out1a
mkfifo /tmp/in1b /tmp/out1b
mkfifo /tmp/in2a /tmp/out2a
mkfifo /tmp/in2b /tmp/out2b
mkfifo /tmp/in3a /tmp/out3a
mkfifo /tmp/in3b /tmp/out3b
 
./bas.tk --auto --ident=node1 --whatwho --dest=NET < /tmp/in1a > /tmp/out1a &
./net.tk --auto --ident=node1 --whatwho --source=BAS < /tmp/in1b > /tmp/out1b &
 
./bas.tk --auto --ident=node2 --whatwho --dest=NET < /tmp/in2a > /tmp/out2a &
./net.tk --auto --ident=node2 --whatwho --source=BAS < /tmp/in2b > /tmp/out2b &
 
./bas.tk --auto --ident=node3 --whatwho --dest=NET < /tmp/in3a > /tmp/out3a &
./net.tk --auto --ident=node3 --whatwho --source=BAS < /tmp/in3b > /tmp/out3b &
 
cat /tmp/out1a > /tmp/in1b &
cat /tmp/out1b | tee /tmp/in1a > /tmp/in2b &
cat /tmp/out2a > /tmp/in2b &
cat /tmp/out2b | tee /tmp/in2a > /tmp/in3b &
cat /tmp/out3a > /tmp/in3b &
cat /tmp/out3b | tee /tmp/in3a > /tmp/in1b &
```

## Liens mobiles

Quand un lien est supp, il est recréer par le script

```shell
#!/bin/bash
 
# Create named pipes, one input and one output per process.
mkfifo in1 out1
mkfifo in2 out2
mkfifo in3 out3
mkfifo in4 out4
mkfifo in5 out5
mkfifo in6 out6
mkfifo in7 out7
 
# Starting the processes
./wha.tk --auto --ident=node1 < in1 > out1 &
./wha.tk --auto --ident=node2 < in2 > out2 &
./wha.tk --auto --ident=node3 < in3 > out3 &
./wha.tk --auto --ident=node4 < in4 > out4 &
./wha.tk --auto --ident=node5 < in5 > out5 &
./wha.tk --auto --ident=node6 < in6 > out6 &
./wha.tk --auto --ident=node7 < in7 > out7 &
 
# Waiting for the link creation (security delay)
sleep 1 
 
# Links creation: output -> input
# 1 -> 2 and 3
cat out1 | tee in2 in3 &
# 2 -> 1, 4 and 5
cat out2 | tee in1 in4 in5 &
# 3 -> 1 and 6
cat out3 | tee in1 in6 &
# 4 -> 2, 5 and 7
cat out4 | tee in2 in5 in7 &
# 5 -> 2 and 4
cat out5 | tee in2 in4 &
# 6 -> 3 and 7
cat out6 | tee in3 in7 &
# 7 -> 4 and 6
cat out7 | tee in4 in6 &
 
# Waiting 10 seconds before changing the topology
sleep 10
 
# Deleting link 5 <-> 2
# Deleting link 5 <-> 4
# Adding link 5 <-> 3
num=`ps aux | grep "cat out2" | grep -v grep | tr -s ' ' | cut -d' ' -f2`
kill -KILL $num
 
num=`ps aux | grep "cat out3" | grep -v grep | tr -s ' ' | cut -d' ' -f2`
kill -KILL $num 
 
num=`ps aux | grep "cat out4" | grep -v grep | tr -s ' ' | cut -d' ' -f2`
kill -KILL $num 
 
num=`ps aux | grep "cat out5" | grep -v grep | tr -s ' ' | cut -d' ' -f2`
kill -KILL $num 
 
# Creating new connections and recreating those that would not have been deleted.
cat out2 | tee in1 in4 &
cat out3 | tee in1 in5 in6 &
cat out4 | tee in2 in7 &
cat out5 | tee in3 &
 
# Waiting two seconds before pursuing
sleep 2
 
# Deleting link 6 <-> 7
num=`ps aux | grep "cat out6" | grep -v grep | tr -s ' ' | cut -d' ' -f2`
kill -KILL $num
 
num=`ps aux | grep "cat out7" | grep -v grep | tr -s ' ' | cut -d' ' -f2`
kill -KILL $num
 
# Re-creating links that would not have been deleted
cat out6 | tee in3 &
cat out7 | tee in4 &
 
# Waiting 20 secondes
sleep 20
 
# Killing all applications
killall wha.tk cat tee
 
# Deleting all named pipes
rm in* out*

```



mkfifo /tmp/in1a /tmp/out1a
mkfifo /tmp/in1b /tmp/out1b
mkfifo /tmp/in2a /tmp/out2a
mkfifo /tmp/in2b /tmp/out2b
mkfifo /tmp/in3a /tmp/out3a
mkfifo /tmp/in3b /tmp/out3b
 
./bas.py --auto --ident=node1 --whatwho --dest=NET < /tmp/in1a > /tmp/out1a &
./net.py --auto --ident=node1 --whatwho --source=BAS < /tmp/in1b > /tmp/out1b &
 
./bas.py --auto --ident=node2 --whatwho --dest=NET < /tmp/in2a > /tmp/out2a &
./net.py --auto --ident=node2 --whatwho --source=BAS < /tmp/in2b > /tmp/out2b &
 
./bas.py --auto --ident=node3 --whatwho --dest=NET < /tmp/in3a > /tmp/out3a &
./net.py --auto --ident=node3 --whatwho --source=BAS < /tmp/in3b > /tmp/out3b &
 
cat /tmp/out1a > /tmp/in1b &
cat /tmp/out1b | tee /tmp/in1a > /tmp/in2b &
cat /tmp/out2a > /tmp/in2b &
cat /tmp/out2b | tee /tmp/in2a > /tmp/in3b &
cat /tmp/out3a > /tmp/in3b &
cat /tmp/out3b | tee /tmp/in3a > /tmp/in1b