from ast import Constant, arg
import sys
import math
import threading
import _thread
import time
from enum import Enum

# def site(arg[1], arg[2]):
#     print("site")
#     print(arg[1])
#     print(arg[2])

source_site_local = 0

nb_sites = 4
tab = [  # tab[i][0]:type de message     tab[i][1]:n° horloge
    ["L", 0],
    ["L", 0],
    ["L", 0],
    ["L", 0]
]

global h
h = 0


class message(Enum):
    type = ""
    h = -1
    source_site = -1
    source_type = ""  # bas / net
    destination = -2  # 99 pour tous les autre, -1 pour ma bas
    contenu = " "
    EtatGlobal = {}


def max(m, n):
    if m >= n:
        return m
    elif m < n:
        return n


def avant_horlogeLogiqueEstampille(date, source_site):
    if(tab[source_site_local][1] < date):
        return 1
    elif(tab[source_site_local][1] == date) and (source_site_local < source_site):
        return 1
    else:
        return 0

# class message_type(Enum):
#     demandeSC = 1
#     debutSC = 2
#     finSC = 3
#     requete = 4
#     liberation = 5
#     accuse = 6
#     prepost = 7
#     rouge = 8
#     blanc = 9


def Initialisation():
    for i in range(len(tab)):
        tab[i] = ["L", 0]

    global h
    h = 0


def ReceptionDemandeSC():
    h = h + 1
    tab[source_site_local][0] = "R"
    tab[source_site_local][1] = h
    print("requete,"+h+","+source_site_local+",99")
    # envoyer( [requête] hi ) à tous les autres sites


def ReceptionFinSC():
    h = h + 1
    tab[source_site_local][0] = "L"
    tab[source_site_local][1] = h

    print("liberation,"+h+","+source_site_local+",99")
    # envoyer( [libération] hi ) à tous les autres sites


def ReceptionRequete(message):
    h = max(h, message.h) + 1
    tab[source_site_local][0] = "R"
    tab[source_site_local][1] = h
    print("accusee,"+h+","+source_site_local+","+message.source_site)
    # envoyer( [accusé] hi ) à Sj

    flag = 1
    if tab[source_site_local][0] == "R":
        for i in range(nb_sites):   # 0 1 2 3
            if i != source_site_local:
                if avant_horlogeLogiqueEstampille(tab[i][1], i) == 0:
                    flag = 0
    else:
        flag = 0

    if(flag == 1):
        print("debutSC,"+h+","+source_site_local+","+"-2")
        # envoyer( [débutSC] ) à l’application de bas


def Receptionliberation():
    h = max(h, message.h) + 1
    tab[source_site_local][0] = "L"
    tab[source_site_local][1] = h

    flag = 1
    if tab[source_site_local][0] == "R":
        for i in range(nb_sites):   # 0 1 2 3
            if i != source_site_local:
                if avant_horlogeLogiqueEstampille(tab[i][1], i) == 0:
                    flag = 0
    else:
        flag = 0

    if(flag == 1):
        print("debutSC,"+h+","+source_site_local+","+"-1")
        # envoyer( [débutSC] ) à l’application de bas


def ReceptionAccuse():
    h = max(h, message.h) + 1
    if tab[message.source_site][0] != "R":
        tab[source_site_local][0] = "ACK"
        tab[source_site_local][1] = h

    flag = 1
    if tab[source_site_local][0] == "R":
        for i in range(nb_sites):   # 0 1 2 3
            if i != source_site_local:
                if avant_horlogeLogiqueEstampille(tab[i][1], i) == 0:
                    flag = 0
    else:
        flag = 0

    if(flag == 1):
        print("debutSC,"+h+","+source_site_local+","+"-1")
        # envoyer( [débutSC] ) à l’application de bas

# def EnvoyerDebutSC():
#     print("debutSC,bas")

# def EnvoyerRequete():
#     print("")

###############################################################################################################################


type = "Blanc"
initiateur = 0
bilan = -1
EtatGlobal = {}
NbEtatAttendus = -1
NbMsgAttendus = -1

n_site_local = 0
nb_sites = 4


def Initialisation(n, initialisateur):
    type = "Blanc"
    initiateur = initialisateur
    bilan = 0
    EtatGlobal = {}
    NbEtatAttendu = 0
    NbMsgAttendus = 0
    n_site_local = n


def DebutInitialisation():
    # if(n_site_local == 0):
    if(initiateur == 1):
        type = "Rouge"
        initiateur = 1
        EtatGlobal = {}
        NbEtatAttendu = nb_sites-1
        NbMsgAttendus = bilan


def ReceptionNet():
    bilan = bilan - 1
    if (message.type == "rouge") and (type == "blanc"):
        EtatGlobal = {}
        print("debutSC,"+h+","+n_site_local+","+"-1")
        print(bilan+",0")
        #envoyer(EtatGlobal, bilan, site_0)
    if (message.type == "blanc") and (type == "rouge"):
        message.type = "prepost"

        #envoyer(message, site_0)


def EnvoyerNet(message):
    bilan = bilan + 1
    print(message)
    #envoyer(message, type)


def ReceptionMsgEtat(message):
    if(initiateur == 1):
        EtatGlobal = EtatGlobal + message.contenu
        NbEtatAttendus = NbEtatAttendus - 1
        NbMsgAttendus = NbMsgAttendus + bilan
        if(NbEtatAttendus == 0 and NbMsgAttendus == 0):
            exit
    else:
        print(message)


def ReceptionMsgPrePost(message):
    if(initiateur == 1):
        NbMsgAttendus = NbMsgAttendus - 1
        EtatGlobal = EtatGlobal + message.contenu
        if(NbEtatAttendus == 0 and NbMsgAttendus == 0):
            exit
    else:
        print(message)


def Reception(message):
    print("reception")
    chaine = message.split(",")
    message.type = chaine[0]
    message.h = chaine[1]
    message.source_site = chaine[2]
    message.source_type = chaine[3]
    message.destination = chaine[4]
    message.contenu = chaine[5]
    message.EtatGlobal = chaine[6]
    if (message.source_type == "net"):
        ReceptionNet(message)

        if(message.type == "demandSC"):
            ReceptionDemandeSC()
        elif(message.type == "finSC"):
            ReceptionFinSC()
        elif(message.type == "requete"):
            ReceptionRequete()
        elif(message.type == "liberation"):     #
            Receptionliberation()
        elif(message.type == "accuse"):
            ReceptionAccuse()
    #elif (message.source_type == "bas"):
        #if(message.type=="debutSC"):      #
        # EnvoyerDebutSC()

        # pas encore fini
        #


def main():
    Initialisation()
    while(1):
        message = sys.stdin.readline()
        Reception(message)

    # print


if __name__ == '__main__':
    # sys.exit(main())
    main()
