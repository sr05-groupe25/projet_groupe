#!/usr/bin/python3
import os
import sys
# from sortedcontainers import SortedDict
sys.path.append(os.path.abspath(
    "{}/LIBAPGpy/LIBAPGpy".format(os.environ["APG_PATH"])))
import libapg as apg

# Global variables
srcapp = "appnet"

class NETMessage(apg.msg.Message):
    """Application-specific message treatment"""

    def __init__(self, text, app, payload=None, nseq=None):
        super().__init__(text, app)
        self.fields += ["player","typeMsg","x","y","ok_sc","horloge"]
        self.fields += [srcapp]
        self.parse_text(text)

    def is_local(self):
        return not srcapp in self.content  # pour vérifier si "appnet" est dans les clés
    def have_x(self):
        return "X" in self.content  # pour vérifier si "X" est dans les clés
    def have_y(self):
        return "Y" in self.content  # pour vérifier si "Y" est dans les clés


class SAVEMessage(apg.msg.Message):
    """Application-specific message treatment"""
    def __init__(self, text, app, payload=None, nseq=None):
        super().__init__(text, app)
        self.fields += ["typeMsg",""]
        if payload != None:
            self.content["payload"] = payload
        if nseq != None :
            self.content["nseq"] = nseq
        if len(text) > 0:
            self.parse_text(text)
    def payload(self):
        return self.content["payload"]
    def nseq(self):
        return self.content["nseq"]



class NETApp(apg.Application):
    def __init__(self):
        # default_options_values={"appname":"NET"}
        default_options_values = {'whatwho': True, "appname": "NET"}
        super().__init__(default_options_values)

        self.mandatory_parameters += []  # No mandatory parameter for this app
        # self.destination_app=self.APP()
        # self.destination_zone=self.com.hst_air()
        self.received_message = ""
        self.msg_bas = ""
        self.msg_net = ""
        self.sent_message = ""

        # Initialiser les attributs de NET
        # self.payload1 = apg.msg.SortedDict()  # ok_sc~OK/NO index_x~1 index_y~0 n_site~n  horloge~h
	    
        # self.nseq = 0
        self.nbOK = 0
        self.ok_sc = ""
        self.nbJoueur = self.params["nbJ"]

        # self.id = self.params["id"]
        # self.case_local_demande_x = -1  # [-1,-1]
        # self.case_local_demande_y = -1
        # self.case_else_site_demande_x = -1  # [-1,-1]
        # self.case_else_site_demande_y = -1
        # self.horloge_local = -1
        # self.horloge_received = -1  # supprimer
        # self.site_local = -1
        # self.site_received = -1
        self.is_mort = 0

        ## save_zone zone
        self.gui.tk_instr("""
self.save_zone = tk.Frame(self.root)
self.save_local_button = tk.Button(self.save_zone, text="Save local", 
command=self.app().save_local_button, activebackground="red", foreground="red", width=10)
self.save_local_button.pack(side="right")
self.save_global_button = tk.Button(self.save_zone, text="Save global", 
command=self.app().save_global_button, activebackground="red", foreground="red", width=10)
self.save_global_button.pack(side="right")
""")
        self.gui.tk_instr('self.save_zone.pack(fill="both", expand="yes", side="top", pady=5)')

        if self.check_mandatory_parameters():
            self.config_gui()
            self.end_initialisation()
            # N° site

        # retirer les dictionnaires dans payload1

    def receive(self, pld, src, dst, where):  # appele quand il y a de nouveau message
        """When a message is received """
        if self.started and self.check_mandatory_parameters():
            self.vrb("{}.rcv(pld={}, src={}, dst={}, where={})".format(
                self.APP(), pld, src, dst, where), 6)  # self.APP() return nom d'app; src = resource de message
            # Useful for logs management, mostly
            super().receive(pld, src=src, dst=dst, where=where)
            self.received_message = NETMessage(pld, self)

            # msg de BAS
            if self.received_message.is_local() and src != self.APP():
                self.msg_bas = self.received_message
                self.gui.tk_instr("""self.received_msg.config(text="Received from {} : {}")""".format(
                    src, self.received_message))
                # ajouter un champ  " ^appnet~BAS^ "
                # self.received_message.content[srcapp] = src
                # self.snd("{}".format(self.received_message),
                #         who=self.APP(), where=self.com.hst_air())
                # self.gui.tk_instr('self.transmitted_msg.config(text="Message transmitted to {} : {}")'.format(
                #     self.APP(), self.received_message))

                # self.parse_payload1_de_BAS(pld)

                if self.msg_bas.content["Pqyload"] == "MORT":
                    self.msg_bas.receive_mort(src)
                elif self.msg_bas.have_x and self.msg_bas.have_y:
                    self.receive_demande_sc_BAS(src)
                else:
                    self.received_message.content[srcapp] = src
                    self.snd("{}".format(self.received_message),
                             who=self.APP(), where=self.com.hst_air())

                self.gui.tk_instr('self.transmitted_msg.config(text="Message transmitted to {} : {}")'.format(
                    self.APP(), self.received_message))

            # msg de tous les autres NETS
            elif not self.received_message.is_local() and src == self.APP():
                self.msg_net = self.received_message
                self.gui.tk_instr("""self.received_msg.config(text="Received from {} : {}")""".format(
                    self.APP(), self.received_message))
                destination_app = self.received_message.content.pop(
                    srcapp, None)  # pop => destination_app = content[srcapp]
                # destination_app = self.received_message.content[srcapp]

                # msg vient d'autre NET à BAS
                if destination_app != None:
                    # self.snd("{}".format(self.received_message),
                    #          who=destination_app, where=self.com.hst_lch())
                    # self.gui.tk_instr('self.transmitted_msg.config(text="Message transmitted to {} : {}")'.format(
                    #     destination_app, self.received_message))

                    # self.parse_payload1_de_NET(pld)

                    if self.ok_sc != "":
                        self.enter_sc(src)
                    elif self.msg_net.have_x and self.msg_net.have_y:
                        self.receive_demande_sc_NET(src)
                        # self.case_else_site_demande_x = -1
                        # self.case_else_site_demande_y = -1
                    else:
                        self.snd("{}".format(self.received_message),
                                 who=destination_app, where=self.com.hst_air())
                        #who=self.APP()
                    self.gui.tk_instr('self.transmitted_msg.config(text="Message transmitted to {} : {}")'.format(
                        destination_app, self.received_message))
                # msg pas besoin d'envoyer au BAS
                else:
                    self.vrb_dispwarning("No destination app")
            else:
                self.vrb_dispwarning(
                    "Unused message '{}' (probably syntax error) from {} {}".format(pld, src, where))
        else:
            self.vrb_dispwarning(
                "Application {} not started".format(self.APP()))

    # obtenir un dictionnaire par payload1
"""     def parse_payload1_de_BAS(self, pld):
        string = pld
        if string[0] == " ":
            del(string[0])
        if string[-1] == " ":
            del(string[-1])
        msg = string.split(" ")
        for elt in msg:
            l = str(elt).split("~", 1)
            if len(elt) > 0:
                self.payload1[l[0]] = l[1]

        # Les attributs attribues par payload1
        if ("ok_sc" in self.received_message.content):
            self.ok_sc = self.payload1["ok_sc"]
        if ("index_x" in self.received_message.content):
            self.case_local_demande_x = self.payload1["index_x"]
        if ("index_y" in self.received_message.content):
            self.case_local_demande_y = self.payload1["index_y"]
        if ("horloge" in self.received_message.content):
            self.horloge_local = self.payload1["horloge"]
        if ("n_site" in self.received_message.content):
            self.site_local = self.payload1["n_site"] """

        # NET: pld->self.payload1 (string->dictionnaire)
"""     def parse_payload1_de_NET(self, pld):
        string = pld
        if string[0] == " ":
            del(string[0])
        if string[-1] == " ":
            del(string[-1])
        msg = string.split(" ")
        for elt in msg:
            l = str(elt).split("~", 1)
            if len(elt) > 0:
                self.payload1[l[0]] = l[1]

        # Les attributs attribues par payload1
        if ("index_x" in self.received_message.content):
            self.case_else_site_demande_x = self.payload1["index_x"]
        if ("index_y" in self.received_message.content):
            self.case_local_demande_y = self.payload1["index_y"]
        if ("horloge" in self.received_message.content):
            self.horloge_received = self.payload1["horloge"]
        if ("n_site" in self.received_message.content):
            self.site_received = self.payload1["n_site"]
        if ("is_mort" in self.received_message.content):
            self.is_mort = self.payload1["is_mort"] """

    # Quand on recoit une reponse de SC par les autres NETs, on repond a BAS par Yes/No
    def enter_sc(self, src):
        if self.ok_sc == "NO":  # Mon BAS ne peut pas entrer dans SC
            # self.message = NETMessage(self.ok_sc, self)
            self.message = "NO"
            self.snd(str(self.message), who="BAS", where=self.destination_zone)
        elif self.ok_sc == "YES":  # Mon BAS peut entrer dans SC
            self.nbOK = self.nbOK + 1
            if self.nbOK == self.nbJoueur - 1:
                # self.message = NETMessage(self.ok_sc, self)
                self.message = "YES"
                self.snd(str(self.message), who="BAS",
                         where=self.destination_zone)
                # self.case_local_demande_x = -1
                # self.case_local_demande_y = -1
                self.nbOK = 0

    # Quand Bas demande s'il peut entrer dans SC, on distribue aux tous les NETs
    def receive_demande_sc_BAS(self, src):
        #self.received_message.content[srcapp] = "NET"
        self.received_message.content[srcapp] = src
        self.snd("{}".format(self.received_message),
                 who="NET", where=self.com.hst_air())

    # Quand un autre NET demande s'il peut entrer dans SC, on repond par Yes/No
    def receive_demande_sc_NET(self, src):
        
        if self.msg_net.content["X"] == self.msg_bas.content["X"] and self.msg_net.content["Y"] == self.msg_bas.conent["Y"]:

            # Mon horloge et l'horloge de message recu sont identiques
            if self.horloge_local == self.horloge_received:
                if self.msg_bas.content["Player"] < self.msg_net.content["Player"]:  # Mon n° de site est plus petit
                    self.received_message.content["ok_sc"] = "OK"  # les autres peuvent entrer
                    """ for elt in self.payload1:
                        self.message = self.message + elt + \
                            '~' + self.payload1[elt] + " " """
                    self.snd(str(self.message), who="NET",
                             where=self.destination_zone)
                else:
                    # les autres ne peuvent pas entrer
                    self.received_message.content["ok_sc"] = "NO"
                    """ for elt in self.payload1:
                        self.message = self.message + elt + \
                            '~' + self.payload1[elt] + " " """
                    self.snd(str(self.message), who="NET",
                             where=self.destination_zone)

            # Mon horloge est plus petit que l'horloge de message recu
            elif self.msg_bas.content["horloge"] < self.msg_net.content["horloge"]:
                self.received_message.content["ok_sc"] = "NO"
                """ for elt in self.payload1:
                    self.message = self.message + elt + \
                        '~' + self.payload1[elt] + " " """
                #self.message = NETMessage(self.pld, self)
                self.snd(str(self.message), who="NET",
                         where=self.destination_zone)
            # Mon horloge est plus petit que l'horloge de message recu
            elif self.msg_bas.content["horloge"] > self.msg_net.content["horloge"]:
                self.received_message.payload1["ok_sc"] = "OK"
                """ for elt in self.payload1:
                    self.message = self.message + elt + \
                        '~' + self.payload1[elt] + " " """
                #self.message = NETMessage(self.pld, self)
                self.snd(str(self.message), who="NET",
                         where=self.destination_zone)
        else:
            self.received_message.content["ok_sc"] = "OK"
            """ for elt in self.payload1:
                self.message = self.message + elt + \
                    '~' + self.payload1[elt] + " " """
            self.snd(str(self.message), who="NET", where=self.destination_zone)

    def receive_mort(self, src):
        self.nbJoueur = self.nbJoueur - 1
        # self.received_message.content[srcapp] = "NET"
        self.received_message.content[srcapp] = src
        self.snd("{}".format(self.received_message),
                 who="NET", where=self.com.hst_air())
    
    def save_local_button(self):
        """ When send button on app area is pushed """
        msg = "Save local"
        destination_app = "BAS"
        message = SAVEMessage("",self, msg, self.nseq)
        self.snd(str(message), who=destination_app)
        self.nseq += 1
        # self.sending_in_progress = self.loop.call_later(float(self.period),self.send_button,graphic_msg,graphic_who, graphic_where,graphic_period)
        # self.gui.tk_instr('self.sending_button.config(text="sending...")')
    
    def save_global_button(self):
        """ When send button on app area is pushed """
        msg = "Save global"
        destination_app = "NET"
        message = SAVEMessage("",self, msg, self.nseq)
        self.snd(str(message), who=destination_app)
        self.nseq += 1
        # self.sending_in_progress = self.loop.call_later(float(self.period),self.send_button,graphic_msg,graphic_who, graphic_where,graphic_period)
        # self.gui.tk_instr('self.sending_button.config(text="sending...")')

    # interface tk
    def config_gui(self):
        # Interface tk
        self.gui.tk_instr("""
self.app_zone = tk.LabelFrame(self.root, text="{}")
self.received_msg = tk.Label(self.app_zone,text="{}")
self.transmitted_msg = tk.Label(self.app_zone,text="{}")
self.received_msg.pack(side="top")
self.transmitted_msg.pack(side="top")
self.app_zone.pack(fill="both", expand="yes", side="top", pady=5)
""".format(self.APP(), self.received_message, self.sent_message))  # Graphic interface (interpreted if no option notk)


app = NETApp()
if app.params["auto"]:
    app.start()
else:
    app.dispwarning("app not started")
