#!/usr/bin/python3
from difflib import restore
from errno import EEXIST
from functools import total_ordering
from operator import truediv
import os
import sys
from termios import TIOCPKT_DOSTOP
from turtle import goto
sys.path.append(os.path.abspath("{}/LIBAPGpy/LIBAPGpy".format(os.environ["APG_PATH"])))
import libapg as apg

#Global variables
srcapp="appctr"

class CTRMessage(apg.msg.Message):
    """Application-specific message treatment"""
    def __init__(self, text, app, payload=None, additionnalFields={}):
        super().__init__(text, app)
        self.fields += [srcapp, "payload", "nseq", "typeMsg", "player", "x", "y", "status", "horloge", "W", "G", "P", "Hvectoriellle"]

        if (payload != None):
            self.content["payload"] = payload
        
        for field in additionnalFields:
            self.content[field] = additionnalFields[field]

        if len(text) > 0:
            self.parse_text_with_known_fields(text)

    def is_local(self):
        return not srcapp in self.content 

    
    # Getter du contenu du message (MORT OR MVT SC LSAVE GSAVE NBJ)
    def payload(self): 
        return self.content["payload"]

    # Getter du numéro de séquence du message
    def nseq(self):
        return self.content["nseq"]

    def player(self):
        return self.content["player"]

    def x(self):
        return self.content["x"]
    
    def y(self):
        return self.content["y"]

    def status(self):
        return self.content["status"]

    def horloge(self):
        return self.content["horloge"]

    def w(self):
        return self.content["w"]
    
    def G(self):
        return self.content["G"]

    def P(self):
        return self.content["p"]

    def Hvectoriellle(self):
        return self.content["Hvectoriellle"]

 
class CTRApp(apg.Application):
    # *************************************************************************************************
    # PARTIE INIT
    # *************************************************************************************************
    def __init__(self):
        # Initialisation
        default_options_values={'whatwho':True,"appname":"CTR"}
        super().__init__(default_options_values)

        self.mandatory_parameters += ["nbJ", "ident"] # No mandatory parameter for this app

        self.received_message=""
        self.sent_message=""
        self.horloge = 0
        self.tabMessages = []

        # les attributs pour l'algo de la sauvegarde
        self.NbEtatsAttendus = 0
        self.couleur = "blanc"
        self.initiateur = False 
        self.etatGlobale = [] 
        self.coherance = False

        # attribut pour l'algo du calcul de la choérence
        self.horlogeMarice = []
        self.etreCoherence = True
        self.NbHvectorielleAttendus = 0


        if self.check_mandatory_parameters():
            
            # Création d'un tableau de dictionnaire pour mémoriser les derniers messages
            for i in range (int(self.params["nbJ"])):
                self.tabMessages.append({"msg":"REQSC", "h":0, "seq":0, "info":""})

            print(self.tabMessages)
            self.i = int(self.params["ident"][1]) - 1

            self.config_gui()
            self.end_initialisation()

    def check_mandatory_parameters(self):
        super().check_mandatory_parameters()

        if not self.params["ident"] in ["J1","J2","J3","J4"]:
            self.vrb_disperror("ident doit correspondre à J1, J2, J3 ou J4.")
            return False

        i = int(self.params["ident"].replace("J", "")) - 1
        if i > int(self.params["nbJ"]):
            self.vrb_disperror("ident être inférieur ou égal au nombre de joueurs.")
            return False

        if not int(self.params["nbJ"]) in [1,2,3,4]:
            self.vrb_disperror("nbJ doit être compris en 1 et 4 inclus")
            return False  

        return True

    def getInitPosition(self, corner) :
        if corner == 1: return [0,0]
        elif corner == 2: return [0, 9]
        elif corner == 3: return [9, 0]
        elif corner == 4: return [9, 9]
        

    def updateTab(self, player, type, seq, info = ""):
        # Récupération du numéro du joueur
        i = int(player.replace("J", "")) - 1

        # Ajout du message dans notre mémoire
        self.tabMessages[i]["msg"] = type
        self.tabMessages[i]["h"] = self.horloge
        self.tabMessages[i]["seq"] = seq
        self.tabMessages[i]["info"] = info

    def updateEtatglobal (self, player, Xposition, Yposition, Wposition, Gosition, Pposition):
        # Récupération du numéro du joueur
        i = int(player.replace("J", "")) - 1

        # Ajout de la position dans notre mémoire
        self.etatGlobale[i]["X"] = Xposition
        self.etatGlobale[i]["Y"] = Yposition
        self.etatGlobale[i]["W"] = Wposition
        self.etatGlobale[i]["G"] = Gosition
        self.etatGlobale[i]["P"] = Pposition

    def updateHorlogemarice (self, player, Hvectoriellle):
        # Récupération du numéro du joueur
        j = int(player.replace("J", "")) - 1

        # Ajout de l'horloge victorielle dans la matrice
        for i in range (int(self.params["nbJ"])):
            self.horlogeMarice[i][j] = int(Hvectoriellle[i])
        
        self.NbHvectorielleAttendus += 1



    
    # *************************************************************************************************
    # PARTIE RECEPTION
    # *************************************************************************************************
    def receive(self, pld, src, dst, where):
        """When a message is received """
        if self.started  and self.check_mandatory_parameters():
            # Mode Verbose
            self.vrb("{}.rcv(pld={}, src={}, dst={}, where={})".format(self.APP(),pld, src, dst, where), 6)

            
            
            # Réception d'un message et transformation en ControleMessage (+ affichage sur la fenetre)
            super().receive(pld, src=src, dst=dst, where=where)
            self.received_message=CTRMessage(pld,self)

            self.gui.tk_instr("""self.received_msg.config(text="Received from {} : {}")""".format(src,self.received_message))
            self.vrb("Recu isLocal=%s src=%s selfAPP=%s" % (self.received_message.is_local(), src, self.APP()), 1)

            # Reception d'un message de BAS (WUM)
            if self.received_message.is_local() and src != self.APP():
                self.processBASMessage(self.received_message, src)
            
            # Réception d'un message de NET (CTR)
            elif not self.received_message.is_local() and src == self.APP():
                destination_app = self.received_message.content.pop(srcapp, None)

                # Retransmission du message à la destination voulu (self ou BAS)
                if destination_app != None:
                    self.processNETMessage(destination_app, self.com.hst_lch(), src)
                else:
                    self.vrb_dispwarning("No destination app")
            else:
                self.vrb_dispwarning("Unused message '{}' (probably syntax error) from {} {}".format(pld, src, where))
        else:
            self.vrb_dispwarning("Application {} not started".format(self.APP()))
    

    # Traitement des messages reçus de BAS
    def processBASMessage(self, msg : CTRMessage, src):
        self.received_message.content[srcapp] = src
        type = msg.payload()

        # Bloc qui traite le message si il nous concerne

        # Demande SC > géré par NET
        if (type == "SC"):
            self.receiveSC_bas(msg.player(), msg.x(), msg.y(), msg.nseq())

        # Gestion des sauvegardes > géré par NET
        elif (type == "LSAVE"):
            self.receiveLocalSave_bas(msg.status(),)

        # A voir TODO
        elif (type == "GSAVE"):
            self.receiveGlobalSave_bas(msg.player(), msg.x(), msg.y(), msg.W(), msg.G(), msg.P())
            

        # Sinon, le message ne nous concerne pas (OR, MORT, MVT)
        else: # Retransmission aux autres NET (CTR)
            self.retransmission(self.APP(), self.com.hst_air())
    
        # Traitement des messages reçus de NET
    def processNETMessage(self, dest, where, src):
        type = self.received_message.payload()
        msg = self.received_message

        # Bloc qui traite le message si il nous concerné

        # Demande d'une ressource critique par un autre net
        if (type == "REQSC"):
            self.receiveREQSC_net(msg.horloge(), msg.player(),msg.nseq(), msg.x(), msg.y(), src)

        # Demande SC > géré par NET
        elif (type == "ACC"):
            self.receiveACC_net(msg.player(), msg.horloge(), msg.x(), msg.y())

        # A voir TODO
        elif (type == "GSAVE"):
            self.receiveGlobalSave_net()
            # self.NbEtatsAttendus += 1
        
        # Demande d'une horloge vectorielle par un autre net
        elif (type == "REQHvect"):
            self.sendHorlogevetorielle()
            
        # Récéption d'une horloge vectorielle par un autre net
        elif (type == "RECHvect"):
            self.receiveHorlogevectorielle_net(msg.player(), msg.Hvectoriellle())

        # Sinon, le message ne nous concerne pas
        else: # Retransmission à BAS
            self.sendMessage(dest, where)

    # Réception d'une demande de SC de BAS
    def receiveSC_bas(self, player, x, y, seq) :
         # Incrémentation de l'horloge locale
        self.horloge += 1

        self.updateTab(self.params["ident"], "REQSC", seq)
        self.sendREQSC_net(player, x, y, seq)

    # Réception d'une demande de SC de NET
    def receiveREQSC_net(self, horloge, player, seq, x, y, src):
        self.horloge = max(self.horloge, int(horloge)) + 1
        self.updateTab(player, "REQSC", seq)

        self.sendACC_net(src, player, x, y)

        self.checkRequest(x, y)

    def receiveACC_net(self, player, horloge, x, y):
        self.horloge = max(self.horloge, int(horloge)) + 1

        j = int(player.replace("J", "")) - 1

        # Si ce n'était pas une requête, on écrase
        if (self.tabMessages[j]["msg"] != "REQSC"):
            self.updateTab(player, "ACC", 0)
        
        self.checkRequest(x, y)

    def jeSuisPlusPetitQue(self,  j):
        # Comparaison des horloges
        if (self.tabMessages[self.i]["h"] < self.tabMessages[j]["h"]): # Mon horloge est plus petite
            return True
        elif(self.tabMessages[self.i]["h"] > self.tabMessages[j]["h"]): # Mon horloge est plus grande
            return False
        
        # Comparaison des Sites
        else:
            if (self.i < j): # Mon site est plus petit
                return True
            else: # Mon site est plus grand (égalité impossible)
                return False

    # on check une requête pour la case (x, y)
    def checkRequest(self, x, y):
        # On vérifie notre propre requête, si c'est sur la même case
        reqPlusRecenteTrouvée = False
        if (self.tabMessages[self.i]["msg"] == "REQSC" and self.tabMessages[self.i]["info"] ==  "%s,%s".format(x, y)):
            for k in range (self.tabMessages):
                # si on est plus ancien, on "gagne"
                if (k != self.i and self.jeSuisPlusPetitQue(k)):
                    reqPlusRecenteTrouvée = True
                    break
        
        if not reqPlusRecenteTrouvée:
            self.sendSC_bas(x, y)

    def receiveGlobalSave_net(self):
        # self.couleur = "rouge"
        if self.couleur == "blanc":
            self.sendGlobalSave_bas()
        elif self.couleur == "rouge":
            if self.initiateur:
                self.sendGlobalSave_bas()
            else:
                pass # pour ce programme, rien à faire, paarceque prépost message ne peut pas exist

    def receiveGlobalSave_bas(self, player, x, y, w, g, p):
        if self.initiateur:
            self.etatGlobale = self.updateEtatglobal(player, x, y, w, g, p)
            self.NbEtatsAttendus += 1
            if self.NbEtatsAttendus == int(self.params["nbJ"]):
                self.sendRequestHorloge_net()
        elif not self.initiateur:
            self.couleur = "rouge"
            self.retransmission(self.APP(), self.com.hst_air())

    def receiveHorlogevectorielle_net(self,player, Hvectoriellle):
        self.updateHorlogemarice(self, player, Hvectoriellle)

        if self.NbHvectorielleAttendus == int(self.params["nbJ"]):
            self.checkSave()
            if not self.etreCoherence:
                self.vrb_disperror("La coupure n'est pas cohérente")

    def checkSave(self):
        for i in range (int(self.params["nbJ"])):
            for j in self.horlogeMarice[i]:
                if j > self.horlogeMarice[i][i]:
                    self.etreCoherence = False
                    break
            # Si on a recu toute les save
            # Calcul de cohérence 

    def receiveLocalSave_bas(self, status):
        if status == "OK":
            self.couleur = "rouge"
        elif status == "NO":
            self.couleur = "blanc"
        
        
    # *************************************************************************************************
    # PARTIE ENVOI
    # *************************************************************************************************
    def retransmission(self, dest, where):
        self.sendMessage(dest, where, self.received_message)

    def sendMessage(self, dest, where, msg : CTRMessage):
        msg.content["horloge"] = self.horloge
        self.snd("{}".format(msg), who=dest, where=where)
        self.gui.tk_instr('self.transmitted_msg.config(text="Message transmitted to {} : {}")'.format(dest, self.received_message))

    # Envoyer une demande de ressource critique aux autres NET
    def sendREQSC_net(self, player, x, y, seq):
        msg = CTRMessage("",self, "REQSC", {"player":player, "x":x, "y":y, "nseq":seq})
        msg.content[srcapp] = self.APP() 
        self.sendMessage(self.APP(), self.com.hst_lch(), msg)

    # Envoyer à bas l'autorisation pour la ressource critique
    def sendSC_bas(self, x, y):
        msg = CTRMessage("",self, "SC", {"status":"OK", "x":x, "y":y})
        self.sendMessage("WUM", self.com.hst_lch(), msg)

    def sendACC_net(self, src, player, x, y):
        msg = CTRMessage("",self, "ACC", {"status":"OK", "player":player,"x":x, "y":y })
        msg.content[srcapp] = src 
        self.sendMessage(src, self.com.hst_lch(), msg)

        # On demande une saveGlobale à tous les NET puis à BAS
    def sendGlobalSave_netbas(self):
        self.couleur = "rouge"
        self.initiateur = True
        msg = CTRMessage("",self, "GSAVE")
        msg.content[srcapp] = self.APP() 
        self.sendMessage(self.APP(), self.com.hst_air(), msg)

        self.sendGlobalSave_bas()

    # On demande une save pour la save globale à notre BAS
    def sendGlobalSave_bas(self):
        msg = CTRMessage("",self, "GSAVE")
        self.sendMessage("WUM", self.com.hst_lch(), msg)

    # On demande à notre BAS de se sauvegarder localement
    def sendLocalSave_bas(self):
        msg = CTRMessage("",self, "LSAVE")
        self.sendMessage("WUM", self.com.hst_lch(), msg)

    def sendRequestHorloge_net(self):
        msg = CTRMessage("",self,"REQHvect")
        self.sendMessage(self.APP(), self.com.hst_lch(), msg)

    def sendHorlogevetorielle(self):
        stringHorloge = ""
        for i in range(int(self.params["nbJ"])):
            stringHorloge += self.tabMessages[i]["h"]
        msg = CTRMessage("",self,"RECHvect",{"Hvectoriellle":stringHorloge})
        self.sendMessage(self.APP(), self.com.hst_lch(), msg)
    

    # *************************************************************************************************
    # PARTIE INTERFACE
    # *************************************************************************************************

    def config_gui(self):
        ## Interface
        self.gui.tk_instr("""
self.app_zone = tk.LabelFrame(self.root, text="{}")

self.received_msg = tk.Label(self.app_zone,text="{}")
self.transmitted_msg = tk.Label(self.app_zone,text="{}")

self.save_local_button = tk.Button(self.app_zone, text="Demander une sauvegarde locale", 
    command=self.app().sendLocalSave_bas, activebackground="green", foreground="black", width=70)
self.save_global_button = tk.Button(self.app_zone, text="Demander une sauvegarde globale",
    command=self.app().sendGlobalSave_netbas, activebackground="green", foreground="black", width=70)

self.received_msg.pack(side="top")
self.transmitted_msg.pack(side="top")
self.save_local_button.pack(side="top")
self.save_global_button.pack(side="top")

self.app_zone.pack(fill="both", expand="yes", side="top", pady=5)
""".format(self.APP(),self.received_message, self.sent_message)) # Graphic interface (interpreted if no option notk)

app = CTRApp()
if app.params["auto"]:
    app.start()
else:
    app.dispwarning("app not started")
