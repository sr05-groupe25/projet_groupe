#!/usr/bin/python3
import os
import sys
sys.path.append(os.path.abspath("{}/LIBAPGpy/LIBAPGpy".format(os.environ["APG_PATH"])))
import libapg as apg
from tkinter import messagebox
import random
from typing import List

# Classe WumpusMessage héritant de la classe Message d'Airplug
class WUMMessage(apg.msg.Message):
    """Application-specific message treatment"""

    # Constructeur de la classe Message
    def __init__(self, text, app, payload=None, nseq=None, additionnalFields={}):
        # Constructeur de la classe parente
        super().__init__(text, app)

        # Paramètre spécifique à l'application
        self.fields += ["payload","nseq", "typeMsg", "player", "x", "y", "status"]

        if payload != None: # TYPE of the message (SC MORT OR MVT LSAVE GSAVE)
            self.content["payload"] = payload

        if nseq != None :
            self.content["nseq"] = nseq
        
        for field in additionnalFields:
            self.content[field] = additionnalFields[field]
        
        if len(text) > 0:
            self.parse_text_with_known_fields(text)
    
    # Getter du contenu du message (MORT OR MVT SC LSAVE GSAVE NBJ)
    def payload(self): 
        return self.content["payload"]

    # Getter du numéro de séquence du message
    def nseq(self):
        return self.content["nseq"]

    def player(self):
        return self.content["player"]

    def x(self):
        return self.content["x"]
    
    def y(self):
        return self.content["y"]

    def status(self):
        return self.content["status"]
            

# Classe de base qui gère le jeu du Wumpus
class WUMApp(apg.Application):
    # *************************************************************************************************
    # PARTIE INIT
    # *************************************************************************************************
    # Constructeur de la classe d'application
    def __init__(self):
        # Valeurs par défaut de notre application
        default_options_values={"default-msg":"","appname":"WUM","whatwho":True,"delay":"1", "demo":1, "n":10}  
        super().__init__(default_options_values)

        # Ajout d'options obligatoires
        self.mandatory_parameters += ["n", "nbJ", "ident"]

        # Configuration des attributs de la classe BAS
        self.msg = ""
        self.destination_app= "NET"
        self.destination_zone=self.com.hst_air()
        self.period = float(self.params["delay"])

        # Configuration des attributs particuliers au jeu du Wumpus
        self.width = 400
        self.height = 400
        
        self.positionSouhaitée = None # Aucune position en cours de "demande" de section critique
        self.finDePartie = False

        self.nbJoueurs = 1
        self.playersPositions = []

        self.sequences = {"OR"  : 0, "MVT" : 0, "MORT" : 0, "SC" : 0, "LSAVE" : 0, "GSAVE" : 0, "NBJ":0}

        # Fin de l'initialisation et de la vérification : lancement de l'interface graphique
        if self.check_mandatory_parameters():
            # Initialisation des paramètres de la carte
            self.n = int(self.params["n"])
            
            self.positionWumpus = [0,0]
            self.positionOr = [0,0]

            self.map = self.random_world(self.n)
            '''
            if (self.params["demo"] == "0"):
                self.map = self.random_world(self.n)
            else: self.map = self.static_world(10)
            '''
            self.DX = self.width // self.n
            self.ray = self.DX*2.5/6
            
            self.nbJoueurs = int(self.params["nbJ"])
            self.i = int(self.params["ident"].replace("J", ""))
            
            # Determination de la position initiale des joueurs en fonction de ident
            for i in range(self.nbJoueurs):
                if (i < self.nbJoueurs): self.playersPositions.append(self.getInitPosition(i+1))
                else: self.playersPositions.append([])
            self.positionJoueur = self.playersPositions[self.i - 1]

            self.vrb(self.params["ident"] + " est initialisé en (" + str(self.positionJoueur[0]) + ", " + str(self.positionJoueur[1]) + ")", 1)

            self.config_gui()
            self.end_initialisation()

    def getInitPosition(self, corner) :
        if corner == 1: return [0,0]
        elif corner == 2: return [0, self.n-1]
        elif corner == 3: return [-self.n-1, 0]
        elif corner == 4: return [-self.n-1, self.n-1]
        
    def check_mandatory_parameters(self):
        super().check_mandatory_parameters()
       
        if int(self.params["n"]) < 10 :
            self.vrb_disperror("n doit être supérieur à 10")
            return False

        if not self.params["ident"] in ["J1","J2","J3","J4"]:
            self.vrb_disperror("ident doit correspondre à J1, J2, J3 ou J4.")
            return False

        i = int(self.params["ident"].replace("J", ""))
        if i > int(self.params["nbJ"]):
            self.vrb_disperror("ident être inférieur ou égal au nombre de joueurs.")
            return False

        if not int(self.params["nbJ"]) in [1,2,3,4]:
            self.vrb_disperror("nbJ doit être compris en 1 et 4 inclus")
            return False  

        if not int(self.params["demo"]) in [0,1]:
            self.vrb_disperror("demo doit être égal à 0 ou 1.")
            return False     

        return True

    def generation_senteur(self,world: List[List[str]], n: int) -> List[List[str]]:
        for i in range(n):
            for j in range(n):
                if "W" in world[i][j]:
                    if i + 1 < n and "S" not in world[i + 1][j]:
                        world[i + 1][j] += "S"
                    if j + 1 < n and "S" not in world[i][j + 1]:
                        world[i][j + 1] += "S"
                    if i - 1 >= 0 and "S" not in world[i - 1][j]:
                        world[i - 1][j] += "S"
                    if j - 1 >= 0 and "S" not in world[i][j - 1]:
                        world[i][j - 1] += "S"
        return world
    
    def generation_vent(self,world: List[List[str]], n: int) -> List[List[str]]:
        for i in range(n):
            for j in range(n):
                if "P" in world[i][j]:
                    if i + 1 < n and "V" not in world[i + 1][j]:
                        world[i + 1][j] += "V"
                    if j + 1 < n and "V" not in world[i][j + 1]:
                        world[i][j + 1] += "V"
                    if i - 1 >= 0 and "V" not in world[i - 1][j]:
                        world[i - 1][j] += "V"
                    if j - 1 >= 0 and "V" not in world[i][j - 1]:
                        world[i][j - 1] += "V"

        return world

    def generation_vide(self,world: List[List[str]], n: int) -> List[List[str]]:
        for i in range(n):
            for j in range(n):
                while len(world[i][j]) < 3:
                    world[i][j] += "_"
        return world

    def random_world(self,n: int):

        ratio_puit = 0.1

        # empty world generation
        world = [[""] * n for i in range(n)]
        

        # Génération du Wumpus
        self.positionWumpus = [random.randrange(self.n), random.randrange(self.n)]
        while self.positionWumpus[0]<2 or self.positionWumpus[1]<2 :
            self.positionWumpus = (random.randrange(n), random.randrange(n))
        x, y = self.positionWumpus
        world[x][y] = "W"

        # Génération de l'Or ++ A cote du Wumpus
        self.positionOr = [random.randrange(self.n), random.randrange(self.n)]
        while self.positionOr == self.positionWumpus:
            self.positionOr = (random.randrange(n), random.randrange(n))
        x, y = self.positionOr
        world[x][y] = "G"

        # Génération des puits
        for i in range(n):
            for j in range(n):
                if i>1 and j>1 and "W" not in world[i][j]:

                    r = random.random()
                    if r < ratio_puit:
                        world[i][j] += "P"

        # Génération des éléments
        world = self.generation_senteur(world, n)
        world = self.generation_vent(world, n)
        world = self.generation_vide(world, n)

        return world

    def static_world(self, n:int):
        pass
    # *************************************************************************************************
    # PARTIE RECEPTION
    # *************************************************************************************************

    # Réception des messages
    def receive(self, pld, src, dst, where):
        if self.started  and self.check_mandatory_parameters():
            # Mode Verbose
            self.vrb("{}.rcv(pld={}, src={}, dst={}, where={})".format(self.APP(),pld, src, dst, where), 6)

            # Réception du message et formattage en WumpusMessage
            super().receive(pld, src=src, dst=dst, where=where)
            received_message = WUMMessage(pld,self)

            self.processMessage(received_message)
            
        else:
            self.vrb_dispwarning("Application {} not started".format(self.APP()))
    

    # Traitement des messages reçus
    def processMessage(self, msg : WUMMessage):
        type = msg.payload()
        self.vrb("TYPE DU MESSAGE RECU : " + type, 1)
        self.vrb("x: " + msg.x() + " y:" + msg.y() + " player: " + msg.player() + " nseq: " + msg.nseq(), 1)

        if (type == "OR") : # L'or a été trouvé par un autre Joueur
            self.receiveGold(msg.player())

        elif (type == "MORT"): # Mort d'un joueur
            self.receiveDeath(msg.player())
            
        elif (type == "MVT"): # Mouvement d'un joueur
            self.receiveMove(msg.player(), msg.x(), msg.y())
            
        elif (type == "LSAVE"): # Demande de sauvegarde locale = écriture dans fichier et envoyer statut OK
            self.sendLocalSave()

        elif (type == "GSAVE"): # Demande de sauvegarde globale = envoyer son état
            self.sendGlobalSave()

        elif (type == "SC"): # Réponse à la demande de section critique = bouger ou bloquer
            self.receiveSCMove(msg.status())
        
        elif (type == "NBJ"): # Réponse à la demande de section critique = bouger ou bloquer
            self.sendNbJoueurs()
        # Sinon, ignorer"""

    # Mort d'un autre Joueur = FIN
    def receiveDeath(self, player):
        if (self.nbJoueurs > 0):
            self.nbJoueurs-=1
            
        
        if (self.nbJoueurs == 0):
            self.display_popup("FIN", "Tous les joueurs sont morts...")
            self.finDePartie = True # Mettre à jour l'interace graphique : Message de Fin de partie

    
    # Déplacement d'un autre Joueur = Bloquer la case
    def receiveMove(self, player, x, y): # MVT 
        old = self.playersPositions[int(player[1]) - 1]
        self.playersPositions[int(player[1]) - 1] = [x, y]
        #new =  self.playersPositions[int(player.replace("J", "")) - 1]
        new =  self.playersPositions[int(player[1]) - 1]
	
        self.vrb(player + " a bougé en case (" + x +"," + y + ")", 1)
        # Mettre à jour l'interface graphique = faire bouger l'autre joueur
        self.move_gui_other(player, int(new[0]) - int(old[0]), int(new[1]) - int(old[1]))

    def move_gui_other(self, player, x, y):
        dx = x*self.DX
        dy = y*self.DX

        self.gui.tk_instr("""self.canvas.move(globals()["self.joueur%s"], %s, %s)""" % (int(player[1])-1, dy, -dx))


    
    # Trésor trouvé par un autre joueur = FIN
    def receiveGold(self, joueur):
        self.display_popup("FIN", joueur + " a trouvé le trésor avant vous.")
        self.finDePartie = True

    def receiveSCMove(self, response):
        if (response == "OK"):
            self.positionJoueur[0] = self.positionSouhaitée [0] 
            self.positionJoueur[1] = self.positionSouhaitée [1] 
            self.sendMove()
        else:
            self.positionSouhaitée = None
            self.display_msg("Mouvement impossible (case en cours de synchronisation)")

    # *************************************************************************************************
    # PARTIE ENVOI
    # *************************************************************************************************
    # Envoi d'un message
    def sendMessage(self, fields = {}):
        self.vrb("Message ENVOYE : " + self.msg, 1)

        message = WUMMessage("",self, self.msg, self.sequences[self.msg], fields)
        self.snd(str(message), who=self.destination_app, where=self.destination_zone)
        self.sequences[self.msg] += 1

    # To NET : Envoi de l'état courant du site
    def sendGlobalSave(self):
        self.msg = "J%s %s %s / W %s %s / G %s %s"
        for i in range(self.nbJoueurs):
            self.msg += str() # TODO
        self.sendMessage({})

    # To NET : Envoi d'un message de confirmation
    def sendLocalSave(self):
        # Ecrire dans un fichier save_J1_date.txt
        # Datetime
        # J1 x y
        # J2 x y
        # W x y
        # P x y x y x y
        # G x y
        self.msg("LSAVE")
        self.sendMessage({"status":"OK"})
        
    # To BAS via NET: Envoi partie gagnée : OR Ji
    def sendGold(self):
        self.msg = "OR"
        self.sendMessage({"player" : self.params["ident"]})
        self.finDePartie = True
    
    # To BAS (and NET) via NET : Envoi mort du Joueur : MORT Ji
    def sendDeath(self):
        self.msg = "MORT"
        self.sendMessage({"player" : self.params["ident"]})
        self.finDePartie = True
    
    def direction(self):
        print(self.positionJoueur, self.positionSouhaitée)
        if (self.positionSouhaitée[1] > self.positionJoueur[1]): return "R"
        elif (self.positionSouhaitée[1] < self.positionJoueur[1]) : return "L"
        elif (self.positionSouhaitée[0] > self.positionJoueur[0]) : return "U"
        else: return "D"

    # To BAS via NET : Envoi positionJoueur après 1 déplacement : MVT Ji x y
    def sendMove(self):
        # Mouvement
        dir = self.direction()
        self.positionJoueur[0] = self.positionSouhaitée [0] 
        self.positionJoueur[1] = self.positionSouhaitée [1] 

        print(dir)
        self.move_gui(dir)

        self.positionSouhaitée = None

        # Message
        self.msg = "MVT"
        self.sendMessage({"player" : self.params["ident"], "x" : str(self.positionJoueur[0]), "y":str(self.positionJoueur[1]) })


    # To NET : Envoi d'une demande de section critique sur une case : SC Ji x y
    def sendSCMove(self, positionSouhaitée): # to NET
        self.positionSouhaitée = positionSouhaitée
        self.msg = "SC"
        # self.sendMessage({"player" : self.params["ident"], "x" : str(positionSouhaitée[0]), "y":str(positionSouhaitée[1])})
    
    # To NET : Envoyer le nombre de Joueurs
    def sendNbJoueurs(self):
        self.msg = "NBJ"
        self.sendMessage({"n":str(self.nbJoueurs)})

    # Vérification du mouvement et envoie de la demande de section critique
    def move(self, event):
        # self.vrb("positionActuelle : " + str(self.positionJoueur[0]) + ", " + str(self.positionJoueur[1]), 1)
        # self.vrb("keyChar : " + event.keysym, 1)
        # Si aucun mouvement déjà demandé et en attente de réponse
        if (self.positionSouhaitée == None and not self.finDePartie):
            # Mouvement haut
            if (event.keysym == "Up" and self.positionJoueur[0] != 0):
                self.sendSCMove([self.positionJoueur[0] + 1, self.positionJoueur[1]])
                self.sendMove()

            # Mouvement Bas
            elif (event.keysym == "Down" and self.positionJoueur[0] != -self.n+1):
                self.sendSCMove([self.positionJoueur[0] - 1, self.positionJoueur[1]])
                self.sendMove()

            # Mouvement Gauche
            elif (event.keysym == "Left" and self.positionJoueur[1] != 0):
                self.sendSCMove([self.positionJoueur[0], self.positionJoueur[1] - 1])
                self.sendMove()

            # Mouvement Droite
            elif (event.keysym == "Right" and self.positionJoueur[1] != self.n-1):
                self.sendSCMove([self.positionJoueur[0], self.positionJoueur[1] + 1])
                self.sendMove()
            else:
                self.display_msg("Mouvement impossible !")
        else :
            self.display_msg("Mouvement en cours de réalisation...")

    # Après mouvement, vérification de la case et envoi, si besoin du message mort/or
    def verify(self):    
        # Case de l'or (gold) = GAGNE 
        if ("G" in self.map[-self.positionJoueur[0]][self.positionJoueur[1]]):
            self.display_popup("Gagné !", "Vous avez trouvé le trésor !", "info") 
            self.sendGold()
        
        # Case du Wumpus = MORT
        elif ("W" in self.map[-self.positionJoueur[0]][self.positionJoueur[1]]):
            self.display_msg("Game Over : Le Wumpus vous a tué.e")
            self.sendDeath()

        # Case du Puit = MORT
        elif ("P" in self.map[-self.positionJoueur[0]][self.positionJoueur[1]]):
            self.display_msg("Game Over : Vous êtes tombé.e dans le puits.")
            self.sendDeath()

        # Case de Senteur des puits
        elif ("V" in self.map[-self.positionJoueur[0]][self.positionJoueur[1]]):
            self.display_msg("Il y a puits tout près...")
        
        # Case d'Odeur du Wumpus
        elif ("S" in self.map[-self.positionJoueur[0]][self.positionJoueur[1]]):
            self.display_msg("Le Wumpus rôde pas loin...")
        
        # Rien sur la case
        else:
            self.display_msg("")

    # *************************************************************************************************
    # PARTIE GUI
    # *************************************************************************************************
    def display_popup(self, title, text, type = "error"):
        if (type == "error"):
            messagebox.showinfo(title, text) 
        else:
            messagebox.showinfo(title, text) 
        # self.gui._onDeleteWindow()
        # bloquer les mvt du joueur

    def display_msg(self, msg) :
        self.gui.tk_instr("""self.can_text.itemconfig(self.text, text="%s")""" % (msg))

    def move_gui(self, dir):
        if dir == "U": #UP
            x = 0
            y = -self.DX
        elif dir == "D": #Down
            x = 0
            y = self.DX
        elif dir == "L" : #Left
            x = -self.DX
            y = 0
        else: #Right
            x = self.DX
            y = 0

        self.gui.tk_instr("""self.canvas.move(globals()["self.joueur%s"], %s,%s)""" % (int(self.params["ident"][1])-1, x, y))
        self.verify()     
    
    def draw_grid(self):
        for y in range(self.n):
            for x in range(self.n):
                self.gui.tk_instr("""
self.canvas.create_line(x * self.DX, y * self.DX,(x + 1) * self.DX, y * self.DX,fill="green", width=1)
self.canvas.create_line(x * self.DX, y * self.DX, x * self.DX, (y + 1) * self.DX, fill="green", width=1)
self.canvas.create_line(0, self.n * self.DX - 1,  self.n * self.DX - 1, self.n * self.DX - 1, fill="green", width=1)
self.canvas.create_line(self.n * self.DX - 1, 0,  self.n * self.DX - 1, self.n * self.DX - 1,fill="green", width=1)
""")

    def save_local_button(self):
        f = open("save_local_%s.txt"%self.params["ident"], "w")
        f.write("%s %s %s \n G %s %s \n W %s %s" %(self.params["ident"],self.playersPositions[int(self.params["ident"][1])-1][0],self.playersPositions[int(self.params["ident"][1])-1][1],self.positionOr[0],self.positionOr[1],self.positionWumpus[0],self.positionWumpus[1]))
        f.close()


    # Configuration de l'interface graphique
    def config_gui(self):
        """ GUI settings """
        self.gui.tk_instr("""
self.app_zone = tk.LabelFrame(self.root, text="{}")
self.emission_zone = tk.LabelFrame(self.app_zone, text="Emission") 


self.DX = {}
self.ray = {}
self.positionJoueur = {}
self.width = {}
self.height = {}
self.n = {}
self.map = {}
self.playersPositions = {}
self.colors = ["red","blue","green","yellow"]


self.can_text = tk.Canvas(self.emission_zone, bg="black", width=400, height=50)
self.can_text.pack()
self.text = self.can_text.create_text( 400/2 , 25 , text = "Bienvenue au jeu du Wumpus",fill="yellow" )


self.canvas = tk.Canvas(self.emission_zone, bg = "black", width = 400, height = 400)

self.save_local_button = tk.Button(self.emission_zone, text="Save local", 
command=self.app().save_local_button, activebackground='red', foreground="red", width=10)
self.save_local_button.pack(side="bottom")

for i in range(len(self.playersPositions)):
    globals()['self.joueur%s' %i] = self.canvas.create_oval(self.playersPositions[i][1]*self.DX + self.DX/2 - self.ray, self.playersPositions[i][0]*self.DX + self.DX/2 - self.ray, self.playersPositions[i][1]*self.DX + self.DX/2 + self.ray, self.playersPositions[i][0]*self.DX + self.DX/2 + self.ray,fill = self.colors[i])

for y in range(self.n):
            for x in range(self.n):
                self.canvas.create_line(x * self.DX, y * self.DX, (x + 1) * self.DX, y * self.DX, fill="green", width=1)
                self.canvas.create_line(x * self.DX, y * self.DX, x * self.DX, (y + 1) * self.DX, fill="green", width=1)
                self.canvas.create_line(0, self.n * self.DX - 1,  self.n * self.DX - 1, self.n * self.DX - 1, fill="green", width=1)
                self.canvas.create_line(self.n * self.DX - 1, 0,  self.n * self.DX - 1, self.n * self.DX - 1, fill="green", width=1)
                           
                           
self.canvas.pack()



# Bind canvas with key events
self.canvas.bind("<Key>", self.app().move)
self.canvas.focus_set()
        
self.emission_zone.pack(side="top", fill=tk.BOTH, expand=1)
self.app_zone.pack(fill="both", expand="yes", side="top", pady=5)
""".format(self.APP(),self.DX,self.ray,self.positionJoueur,self.width,self.height,self.n,self.map,self.playersPositions)) # Graphic interface (interpreted if no option notk)

# Lancement de l'application BASE WUMPUS
app = WUMApp()
if app.params["auto"]:
    app.start()
else:
    app.dispwarning("app not started")

